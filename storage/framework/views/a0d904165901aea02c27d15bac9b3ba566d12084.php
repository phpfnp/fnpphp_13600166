<?php $__env->startSection('content'); ?>
    <h1>Create new Todo</h1>
    <form method="post" action="<?php echo e(url('/todo')); ?>" enctype="multipart/form-data">
        <?php echo csrf_field(); ?>
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="<?php echo e("movie".rand(0,3).rand(0,3).rand(0,9).rand(0,9).rand(0,9)); ?>">
        </div>
        <div class="form-group">
            <label>Content</label>
            <input type="text" name="content" class="form-control" value="<?php echo e(old('content')); ?>">
        </div>
        <div class="form-group">
            <label>Due</label>
            <input type="date" name="due" class="form-control" value="<?php echo e(old('due')); ?>">
        </div>
        <div class="form-group">
            <label>Like</label>
            <input type="number" name="score_view" class="form-control" value="<?php echo e(old('score_view')); ?>">
        </div>
        <div class="form-group">
            <label>File</label>
            <input type="file" name="file" class="form-control" >
        </div>
        <button type="submit">submit</button>
    </form>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\phpfnp_13600166\resources\views/create.blade.php ENDPATH**/ ?>