<?php $__env->startSection('content'); ?>
    <br>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="<?php echo e(url('/')); ?>">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo e(url('/todo/create')); ?>">Add Movie</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
    <br>
    <form method="post" action="<?php echo e(url('/todo/'.$todo->id)); ?>">
        <?php echo e(@csrf_field()); ?>

        <?php echo method_field('PUT'); ?>

        <div class="form-group" style="display: none;">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="<?php echo e($todo->title); ?>">
        </div>
        <div class="form-group" style="display: none;">
            <label>Content</label>
            <input type="text" name="content" class="form-control" value="<?php echo e($todo->content); ?>">
        </div>
        <div class="form-group" style="display: none;">
            <label>Due</label>
            <input type="date" name="due" class="form-control" value="<?php echo e($todo->due); ?>">
        </div>
        <input style="display: none;" type="number" class="form-control" name="scoreView" value="<?php echo e($todo->score_view); ?>" id="postScoreView">

        <button class="btn btn-success form-control" type="submit" onclick="score_view()">LIKE</button>

    </form>

    <div class="card mb-3">

        <img src="<?php echo e(url('uploads/'.$todo->file_name)); ?>" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title"><?php echo e($todo->title); ?></h5>
            <p class="card-text"><?php echo e($todo->content); ?></p>
            <form action="<?php echo e(url('/todo/'.$todo->id)); ?>" method="post" id="form-delete">
                <?php echo method_field('DELETE'); ?>
                <?php echo csrf_field(); ?>
                <a href="<?php echo e(url('/todo/'.$todo->id.'/edit')); ?>" class="btn btn-primary form-control">Edit</a>
                <button class="btn btn-danger form-control" onclick="confirm_delete()" type="button">Delete</button>

            </form>
        </div>
    </div>


    <script>
        function confirm_delete() {
            var text = '<?php echo $todo->title; ?>';
            var confirm = window.confirm('ยืนยันการลบนี้'+text);
            if (confirm) {
                document.getElementById('form-delete').submit();
            }
        }

        function score_view() {
            let scoreView = document.getElementById('postScoreView').value;
            let scoreViews = parseInt(scoreView) + 1;

            document.getElementById('postScoreView').value = scoreViews;
        }

        // window.onload = function () {
        //     let scoreView = document.getElementById('postScoreView').value;
        //     let scoreViews = parseInt(scoreView) + 1;
        //
        //     document.getElementById('postScoreView').value = scoreViews;
        // }

    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\phpfnp_13600166\resources\views/show.blade.php ENDPATH**/ ?>