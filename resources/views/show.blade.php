@extends('layouts.app')
@section('content')
    <br>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/todo/create') }}">Add Movie</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </nav>
    <br>
    <form method="post" action="{{url('/todo/'.$todo->id)}}">
        {{@csrf_field()}}
        @method('PUT')

        <div class="form-group" style="display: none;">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="{{ $todo->title }}">
        </div>
        <div class="form-group" style="display: none;">
            <label>Content</label>
            <input type="text" name="content" class="form-control" value="{{ $todo->content }}">
        </div>
        <div class="form-group" style="display: none;">
            <label>Due</label>
            <input type="date" name="due" class="form-control" value="{{ $todo->due }}">
        </div>
        <input style="display: none;" type="number" class="form-control" name="scoreView" value="{{$todo->score_view}}" id="postScoreView">

        <button class="btn btn-success form-control" type="submit" onclick="score_view()">LIKE</button>
{{--        <a href="{{ url('/') }}" class="btn btn-primary"> Go back</a>--}}
    </form>

    <div class="card mb-3">

        <img src="{{url('uploads/'.$todo->file_name)}}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">{{$todo->title}}</h5>
            <p class="card-text">{{$todo->content}}</p>
            <form action="{{ url('/todo/'.$todo->id) }}" method="post" id="form-delete">
                @method('DELETE')
                @csrf
                <a href="{{ url('/todo/'.$todo->id.'/edit') }}" class="btn btn-primary form-control">Edit</a>
                <button class="btn btn-danger form-control" onclick="confirm_delete()" type="button">Delete</button>

            </form>
        </div>
    </div>


    <script>
        function confirm_delete() {
            var text = '{!! $todo->title !!}';
            var confirm = window.confirm('ยืนยันการลบนี้'+text);
            if (confirm) {
                document.getElementById('form-delete').submit();
            }
        }

        function score_view() {
            let scoreView = document.getElementById('postScoreView').value;
            let scoreViews = parseInt(scoreView) + 1;

            document.getElementById('postScoreView').value = scoreViews;
        }

        // window.onload = function () {
        //     let scoreView = document.getElementById('postScoreView').value;
        //     let scoreViews = parseInt(scoreView) + 1;
        //
        //     document.getElementById('postScoreView').value = scoreViews;
        // }

    </script>
@endsection

