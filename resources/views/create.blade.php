@extends('layouts.app')
@section('content')
    <h1>Create new Todo</h1>
    <form method="post" action="{{url('/todo')}}" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="{{ "movie".rand(0,3).rand(0,3).rand(0,9).rand(0,9).rand(0,9) }}">
        </div>
        <div class="form-group">
            <label>Content</label>
            <input type="text" name="content" class="form-control" value="{{ old('content') }}">
        </div>
        <div class="form-group">
            <label>Due</label>
            <input type="date" name="due" class="form-control" value="{{ old('due') }}">
        </div>
        <div class="form-group">
            <label>Like</label>
            <input type="number" name="score_view" class="form-control" value="{{ old('score_view') }}">
        </div>
        <div class="form-group">
            <label>File</label>
            <input type="file" name="file" class="form-control" >
        </div>
        <button type="submit">submit</button>
    </form>
@endsection
