<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Netnow Movie</title>
</head>
<body>

<div class="container">
    @include('inc.message')
    @if(Auth::check())

        @if(Auth::user()->rule == 'admin')
            <p class= text-primary>{{Auth::user()->name}}  {{Auth::user()->rule}}</p>
        @endif

        @if(Auth::user()->rule == 'member')
            <p class= text-primary>{{Auth::user()->name}}  {{Auth::user()->rule}}</p>
        @endif

        <form action="{{url('/logout')}}" method="post">
            @csrf
            <button>logout</button>
        </form>
    @endif
    <br>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Navbar</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/todo/create') }}">Add Movie</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Dropdown
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                </li>
            </ul>
            <form action="/fetch" method="post" autocomplete="off">
                <div class="form-group">
                    <a href="{{url('/search')}}" class="btn btn-primary form-control">Search Movie</a>
                </div>
                {{ csrf_field() }}
            </form>

        </div>
    </nav>
    <br>
    <div class="row row-cols-1 row-cols-md-3">
        <div class="col mb-4">
            <div class="card h-100" style="width: 18rem; height: 100%; text-align: center">
                <img src="https://cdn.onlinewebfonts.com/svg/img_518650.png" class="card-img-top" alt="...">
                <div class="card-body"><a href="{{url('/todo/create')}}" class="btn btn-success form-control">Add Movie</a></div>
            </div>
        </div>

            @if(count($todos)>0)
                @foreach($todos as $todo)
                    <div class="col mb-4">
                    <div class="card h-100" style="width: 18rem;">
                        <img src="{{url('uploads/'.$todo->file_name)}}" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title">{{$todo->title}}</h5>
                            <p class="card-text">{{$todo->content}}</p>
                            <p>{{$todo->due}}</p>
                            <p>Like: {{$todo->score_view}}</p>
                            <a href="{{url('/todo/'.$todo->id)}}" class="btn btn-primary form-control">View Movie</a>

                        </div>
                    </div>
                    </div>
                @endforeach
            @endif


    </div>
    <hr>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
{{--<script>--}}
{{--    $(document).ready(function(){--}}

{{--        $('#title').keyup(function(){--}}
{{--            var query = $(this).val();--}}
{{--            if(query != '')--}}
{{--            {--}}
{{--                var _token = $('input[name="_token"]').val();--}}
{{--                $.ajax({--}}
{{--                    url:"{{ route('search.fetch') }}",--}}
{{--                    method:"POST",--}}
{{--                    data:{query:query, _token:_token},--}}
{{--                    success:function(data){--}}
{{--                        $('#movieList').fadeIn();--}}
{{--                        $('#movieList').html(data);--}}
{{--                    }--}}
{{--                });--}}
{{--            }--}}
{{--        });--}}

{{--        $(document).on('click', 'li', function(){--}}
{{--            $('#title').val($(this).text());--}}
{{--            $('#movieList').fadeOut();--}}
{{--        });--}}

{{--    });--}}
{{--    $('localSearchSimple').jslocal--}}
{{--</script>--}}



<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>





