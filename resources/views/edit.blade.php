@extends('layouts.app')
@section('content')
    <br>

    <h1>Edit</h1>
    <form method="post" action="{{url('/todo/'.$todo->id)}}">
        {{@csrf_field()}}
        @method('PUT')
        <div class="form-group">
            <label>Title</label>
            <input type="text" name="title" class="form-control" value="{{ $todo->title }}">
        </div>
        <div class="form-group">
            <label>Content</label>
            <input type="text" name="content" class="form-control" value="{{ $todo->content }}">
        </div>
        <div class="form-group">
            <label>Due</label>
            <input type="date" name="due" class="form-control" value="{{ $todo->due }}">
        </div>
        <div class="form-group">
            <label>Like</label>
            <input type="number" class="form-control" name="scoreView" value="{{$todo->score_view}}" id="postScoreView">
        </div>
        <button type="submit">submit</button>
    </form>
@endsection


